/****************************************************************************************************/
/**
  \file         main.c
  \brief
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Sep 5, 2015

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/
// #define F_CPU 16000000UL
/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "Typedefs.h"
#include "TaskScheduler.h"
#include "Systick.h"
#include "Uart.h"
#include "Gpio.h"
#include "Sent.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/
u8 msg[] = "hola mundo cruel";
// u8 msg[] = {0x01,0x02,0x03};

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                      *
*****************************************************************************************************/
volatile u8 u8NewData;
u8 u8Temp[100];
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnTaskE(u8 Param);
void vfnTaskP(u8 Param);
// void vfnTaskO(u8 Param);

void vfnUartCallbackRx(void);
/*****************************************************************************************************
*                                    METADATA INFORMATION                                        *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Definition of FUNCTIONS                                      *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Main function. Initialization of scheduler and first task (System Task Init).
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
int main(void)
{
    /*********************************
     * TASK SCHEDULER INITIALIZATION *
     *********************************/
    vfnTaskSchedulerInit();

    /* Initialize drivers */
    vfnSystickInit();

    /* GPIO */
    vfnGpioInit(&sGpioDriverCfg);
    vfnGpioSet(eGPIO_PORTC, 0);
    vfnGpioSet(eGPIO_PORTC, 1);
    vfnGpioSet(eGPIO_PORTC, 2);
    vfnGpioSet(eGPIO_PORTC, 3);

    /* UART */
    vfnUartInit(&sUartChannelCfg);
    vfnUartSetRxCallback(vfnUartCallbackRx);
    vfnUartEnable();

    /* SENT */
    vfnSentInit();

    /* Enable tasks */
    vfnTaskSchedulerEnableT(tTaskE, 0, 0);
    vfnTaskSchedulerEnableTT(tTaskP, 0, 0, 1000u);

    /* Enable global interrupt */
    sei();

    for(;;)
    {
        /*****************
         * INFINITE LOOP *
         *****************/
        vfnTaskScheduler();
    }

    return 0;
}


void vfnTaskE(u8 Param)
{
    u8 u8Size;

    if(u8NewData)
    {
        u8NewData = 0;

        /* Read RX data */
        u8Size = u16fnUartRx(&u8Temp[0]);
        // u8fnUartTx(u8Size, &u8Temp[0]);

        switch(u8Temp[0])
        {
            case 'A':
                vfnGpioSet(eGPIO_PORTC, 3);
                break;
            case 'B':
                vfnGpioSet(eGPIO_PORTC, 2);
                break;
            case 'C':
                vfnGpioSet(eGPIO_PORTC, 1);
                break;
            case 'D':
                vfnGpioSet(eGPIO_PORTC, 0);
                break;
            case '1':
                vfnGpioReset(eGPIO_PORTC, 3);
                break;
            case '2':
                vfnGpioReset(eGPIO_PORTC, 2);
                break;
            case '3':
                vfnGpioReset(eGPIO_PORTC, 1);
                break;
            case '4':
                vfnGpioReset(eGPIO_PORTC, 0);
                break;
            case 'x':
                vfnSentSetTick(SENT_TICK_3_US);
                break;
            case 'y':
                vfnSentSetTick(SENT_TICK_50_US);
                break;
            case 'z':
                vfnSentSetTick(SENT_TICK_90_US);
                break;
            case 'w':
                u8Size = 6;
                u8Temp[0] = 0xA;
                u8Temp[1] = 2;
                u8Temp[2] = 8;
                u8Temp[3] = 0xA;
                u8Temp[4] = 2;
                u8Temp[5] = 0xB;
                vfnSentSendMsg(u8Size, &u8Temp[0]);
            default:
                break;
        }
    }
}

void vfnTaskP(u8 Param)
{
    vfnGpioToggle(eGPIO_PORTB, 5);
}

void vfnUartCallbackRx(void)
{
    u8NewData = 1;
}